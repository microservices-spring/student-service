package com.example.students;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;



    public StudentEntity saveUser(StudentEntity studentEntity){
        return this.studentRepository.save(studentEntity);
    }

}
