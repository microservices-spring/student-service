package com.example.students.interfaces;

import com.example.students.beans.Book;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name="BOOK-SERVICE/book")
@LoadBalancerClient(name = "LoadBalancer BOOK-SERVICE")
public interface BookRestConsumer {

    @GetMapping("/data")
    String getBookData();

    @GetMapping("/{id}")
    Book getBookById(@PathVariable Integer id);

    @GetMapping("/all")
    List<Book> getAllBooks();

    @GetMapping("/entity")
    ResponseEntity<String> getEntityData();

}
