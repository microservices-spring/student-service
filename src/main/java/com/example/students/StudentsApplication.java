package com.example.students;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


//@SpringBootApplication(exclude = {
//        org.springframework.cloud.client.serviceregistry.ServiceRegistryAutoConfiguration.class,
//        org.springframework.cloud.client.serviceregistry.AutoServiceRegistrationAutoConfiguration.class
//})
//@EnableEurekaClient
@EnableFeignClients
@EnableDiscoveryClient
@EnableCaching
@SpringBootApplication
public class StudentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentsApplication.class, args);
    }
}
