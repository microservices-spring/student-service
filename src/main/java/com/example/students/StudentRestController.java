package com.example.students;

import com.example.students.interfaces.BookRestConsumer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
@Slf4j
public class StudentRestController {

    @Value("${hello.message}")
    private String helloMessage;

    @Autowired
    private BookRestConsumer consumer;

    @Autowired
    private StudentService studentService;

    @PostMapping(value = "/add",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public StudentEntity addUser(@RequestBody StudentEntity studentEntity){
        return this.studentService.saveUser(studentEntity);
    }

    @GetMapping("/data")
    public String getStudentInfo() {
        log.debug("call getStudentInfo");

        System.out.println(consumer.getClass().getName());  //prints as a proxy class
        return "Accessing from STUDENT-SERVICE ==> " +consumer.getBookData();
    }

    @GetMapping("/allBooks")
    public String getBooksInfo() {
        log.debug("call getBooksInfo");

        return "Accessing from STUDENT-SERVICE ==> " + consumer.getAllBooks();
    }

    @GetMapping("/getOneBook/{id}")
    @Operation( summary = "Get Book",  description = "Getting book by id which has student", responses = {
            @ApiResponse(description = "Successful Operation", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = String.class))),
            @ApiResponse(description = "Not found", responseCode = "404", content = @Content),
            @ApiResponse(description = "Authentication Failure",responseCode = "401", content = @Content(schema = @Schema(hidden = true))) })
    @Parameter(name = "id", examples = {@ExampleObject(name = "id 1", value = "1"), @ExampleObject(name = "id 2", value = "2")}, required = true)
    public String getOneBookForStd(@PathVariable Integer id) {
        log.debug("call getOneBookForStd - id :{}", id);

        return "Accessing from STUDENT-SERVICE ==> " + consumer.getBookById(id);
    }

    @GetMapping("/entityData")
    public String printEntityData() {
        log.debug("call entityData");
        ResponseEntity<String> resp = consumer.getEntityData();
        return "Accessing from STUDENT-SERVICE ==> " + resp.getBody() +" , status is:" + resp.getStatusCode();
    }


    @GetMapping("/hello")
    public String sayHello() {
        return this.helloMessage;
    }
}
